
// RSADlg.h : ���� ���������
//


#pragma once
#include <vector>

// ���������� ���� CRSADlg
class CRSADlg : public CDialogEx
{
// ��������
public:
	CRSADlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_RSA_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString PublicText;
	CString CipherText;
	CString DeCipherText;
	CString Public_int;

	std::vector <int> public_text;
	std::vector <int> cipher_text;
	std::vector <int> decipher_text;
	long long int n;
	unsigned __int64 public_key;
	unsigned __int64 private_key;
	afx_msg void OnBnClickedCiph();
	afx_msg void OnBnClickedCreateKeys();
	int gcd(int a, int b);
	bool test_ferma(long long int x);
	unsigned __int64 step_mod(long long int base, long long int exp, long long int module);
	long long int evclid(long long int a, long long int b, long long int *x, long long int *y);
	afx_msg void OnBnClickedDeCiph();

	
	
	CString Cipher_int;
	CString Deciph_int;
};
