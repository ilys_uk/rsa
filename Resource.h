//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется RSA.rc
//

#define IDD_RSA_DIALOG                  102
#define IDR_MAINFRAME                   128
#define IDC_PUBLICTEXT                  1001
#define IDC_CIPHERTEXT                  1002
#define IDC_DE_CIPHERTEXT               1003
#define IDC_PUBLIC_KEY                  1004
#define IDC_PRIVATE_KEY                 1005
#define IDC_CIPH                        1006
#define IDC_CREATE_KEYS                 1007
#define IDC_CIPH2                       1008
#define IDC_DE_CIPH                     1008
#define IDC_PUBLICTEXT2                 1009
#define IDC_CIPHERTEXT2                 1010
#define IDC_DE_CIPHERTEXT2              1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
