
// RSADlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "RSA.h"
#include "RSADlg.h"
#include "afxdialogex.h"
#include "time.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ���������� ���� CRSADlg



CRSADlg::CRSADlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRSADlg::IDD, pParent)
	, PublicText(_T("Open text"))
	, CipherText()
	, DeCipherText()
	, public_key(0)
	, private_key(0)
	, Public_int(_T(""))
	, Cipher_int(_T(""))
	, Deciph_int(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CRSADlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PUBLICTEXT, PublicText);
	DDX_Text(pDX, IDC_CIPHERTEXT, CipherText);
	DDX_Text(pDX, IDC_DE_CIPHERTEXT, DeCipherText);
	DDX_Text(pDX, IDC_PUBLIC_KEY, public_key);
	DDX_Text(pDX, IDC_PRIVATE_KEY, private_key);
	DDX_Text(pDX, IDC_PUBLICTEXT2, Public_int);
	DDX_Text(pDX, IDC_CIPHERTEXT2, Cipher_int);
	DDX_Text(pDX, IDC_DE_CIPHERTEXT2, Deciph_int);
}

BEGIN_MESSAGE_MAP(CRSADlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_CIPH, &CRSADlg::OnBnClickedCiph)
	ON_BN_CLICKED(IDC_CREATE_KEYS, &CRSADlg::OnBnClickedCreateKeys)
	ON_BN_CLICKED(IDC_DE_CIPH, &CRSADlg::OnBnClickedDeCiph)
END_MESSAGE_MAP()


// ����������� ��������� CRSADlg

BOOL CRSADlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������
	srand(time(NULL));

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CRSADlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CRSADlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CRSADlg::OnBnClickedCiph()
{
	UpdateData();
	CipherText.Empty();
	Public_int.Empty();
	Cipher_int.Empty();

	public_text.clear();
	cipher_text.clear();
	public_text.resize(PublicText.GetLength());
	cipher_text.resize(PublicText.GetLength());
	for (int k = 0; k < PublicText.GetLength(); k++)
	{
		CString str;
		public_text[k] = PublicText.GetAt(k);		
		str.Format(_T("%d"), public_text[k]);

		Public_int += str;
		Public_int += " ";
		cipher_text[k] = step_mod(public_text[k], public_key, n);

		str.Format(_T("%d"), cipher_text[k]);
		Cipher_int += str;
		Cipher_int += " ";

		CipherText.AppendChar(cipher_text[k]);
	}
	UpdateData(FALSE);

}


void CRSADlg::OnBnClickedCreateKeys()
{
	//��������� p,q
	long long int p, q, e, x = 0, y = 0, delitel;;
	do{ p = (rand() + rand() + rand())%65536; } while (!test_ferma(p));

	do{ q = (rand() + rand() + rand()) % 65536; } while (!test_ferma(q));
	
	/*do{ p = rand() % 256; } while (!test_ferma(p));

	do{ q = rand()% 256; } while (!test_ferma(q));*/
	/*p = 19;
	q = 17;*/
	//�������� ������ �������
	n = p*q;
	//�������� ������� ������ �������
	long long int fi = (p - 1)*(q - 1);
	//���������� ����� e
	do{ e = (rand() + rand())%fi; } while (gcd(e, fi) != 1);
	//���������� d
	delitel=evclid(e, fi, &x, &y);	
	///////////////
	public_key = e;
	if (x<0) private_key = x%fi + fi;
	else private_key = x%fi;

	UpdateData(FALSE);
}


int CRSADlg::gcd(int a, int b)
{
	if (b == 0) return a;
	else return gcd(b, a%b);
}


bool CRSADlg::test_ferma(long long int x)
{
	if (x < 2) return false;
	if (x == 2) return true;
	for (int i = 0; i < 100; i++)
	{
		int a = rand();
		if (gcd(a, x) != 1)
			return false;
		if (step_mod(a, x - 1, x) != 1)
			return false;
	}
	return true;
}

unsigned __int64 CRSADlg::step_mod(long long int base, long long int exp, long long int module)
{
	unsigned __int64 result=1;
	//unsigned long long int result = 1;
	if (exp == 0) return result;
	while (exp >= 1)
	{
		if ((exp % 2) == 1)
		{
			result = result%module;
			result *= base%module;
			exp = exp / 2;
			base = ((base % module)*(base%module))%module;
		}
		else
		{
			exp = exp / 2;
			base = ((base % module)*(base%module))%module;
		}
	}
	return result% module;
}

long long int CRSADlg::evclid(long long int a, long long int b, long long int *xx, long long int *yy)
{
	// ���������� a*xx + b*yy = gcd(a, b)
	long long x_last1 = 1, x_0 = 0, y_last1 = 0, y_0 = 1, x, y;
	while (b > 0)
	{
		long long int q = a / b;
		long long int r = a%b;
		a = b;
		b = r;		
		x = x_last1 - q*x_0;
		y = y_last1 - q*y_0;

		x_last1 = x_0;
		y_last1 = y_0;
		x_0 = x;
		y_0 = y;		
	}

	*xx = x_last1;
	*yy = y_last1;
	return a;
}

void CRSADlg::OnBnClickedDeCiph()
{
	UpdateData();
	DeCipherText.Empty();
	Deciph_int.Empty();
	//cipher_text.clear();
	decipher_text.clear();
	decipher_text.resize(CipherText.GetLength());
	cipher_text.resize(CipherText.GetLength());
	for (int k = 0; k < CipherText.GetLength(); k++)
	{
		//cipher_text[k] = CipherText.GetAt(k);
		
		decipher_text[k] = step_mod(cipher_text[k], private_key, n);

		CString str;
		str.Format(_T("%d"), decipher_text[k]);
		Deciph_int += str;
		Deciph_int += " ";
		DeCipherText.AppendChar(decipher_text[k]);
	}
	UpdateData(FALSE);
	
	
	
}

